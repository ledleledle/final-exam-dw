# SSH

#### Login tanpa Password
Untuk server public, tadi saya belum membaca soal dengan benar. Jadi untuk login server public saya menggunakan SSH Key `id_rsa`.<br>
<img src="ss/1.png">

Jadi akan saya bahas dari sini, setelah login ke server public, buat SSH Key dengan perintah `ssh-keygen`. Masukkan inisial nama, misal disini `leon@dumbways`.<br>
<img src="ss/2.png">
Lalu copy SSH Key yang baru dibuat ke server yang dituju dengan perintah `ssh-copy-id`. Setelah proses copy berhasil, tambahkan identitas SSH Key dengan menjalankan perintah `ssh-add *lokasi_private_key*`.

Setelah semua sudah ditambahkan, selanjutnya coba untuk login tanpa password atau ssh key.
<img src="ss/3.png">

Entah mengapa setiap kali saya keluar dari server ssh, saya harus mengulai perintah `ssh-add` lagi dan lagi. Untuk itu saya memiliki inisiatif untuk menambahkan perintah tersebut setiap kali saya membuka jendela terminal SSH. Jadi caranya saya menambahkan 2 baris perintah pada file `.zshrc`.<br>
<img src="ss/8.png">

Jadi perintah diatas yaitu untuk menambahkan credentials ssh pada ssh-agent. Agar saat login ke server lain, ssh dapat dipercaya oleh agen ssh.

#### SSH Keys untuk git dan jenkins
Saya membuat SSH Key baru pada server frontend 1. Dan langsung saya daftarkan pada github.
<img src="ss/4.png">

Jadi untuk ssh key pada server frontend 2 dan backend. Saya hanya mengcopynya secara manual dari server frontend 1. Jadi ke 3 server ini memiliki SSH Key yang sama. Saya akan memberikan contoh pada jenkins saya, dengan 1 private key, namun ke 3 server ini dapat terhubung dengan Jenkins.<br>
<img src="ss/5.gif">

#### Login tanpa IP
Diatas saya sudah membuat login tanpa password. Sekarang saya akan menyingkatnya lagi menjadi tanpa password & ip. Jadi hanya menggunakan inisial yang saya setting sendiri. Nama inisial diberikan pada `Host`, username pada `User` dan IP pada `Hostname`.<br>
<img src="ss/6.png">

Berikut hasilnya, jadi saya hanya perlu memasukkan perintah `ssh inisial`.
<img src="ss/7.gif">