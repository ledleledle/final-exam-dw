# Database

Untuk database disini menggunakan PostgreSQL. Untuk installasinya saya menggunakan docker. Buat volume pada `/home/db/postgre`, buka port 5432, dan ambil image `postgres:10`. Lalu cek status container dengan menjalankan perintah `docker ps`.<br>
<img src="ss/1.png">

Pada folder volume yang sudah dibuat tadi. Edit dengan `sudo`. Untuk file `postgresql.con`, ubah `listen_addresses` menjadi `*`.<br>
<img src="ss/2.png">

Dan untuk file `pg_hba.conf` hanya ubah *METHOD* menjadi `trust`.<br>
<img src="ss/3.png">

Coba masuk ke user `postgres`. Untuk mengetest apakah postgresql sudah berjalan.
```
docker exec -it postgres psql -U postgres
```
Dimulai dari kiri, `docker exec -it` docker akan mengeksekusi perintah, `postgres` nama docker container, `psql -U postgres` login postgresql menggunakan user **postgres**.<br>

<img src="ss/4.png">

Selanjutnya buat user baru.
```
CREATE USER leon WITH PASSWORD 'leon';
```

Lalu buat database `dumbflix`.
```
CREATE DATABASE dumbflix;
```

Lalu beri akses user `leon` pada database `dumbflix`.
```
GRANT ALL PRIVILEGES ON DATABASE dumbflix to leon;
```

Lalu test masuk ke user baru yang telah dibuat.<br>
<img src="ss/5.png">