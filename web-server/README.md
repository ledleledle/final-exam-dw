# Final Exam DW

Untuk web server saya juga mengkonfigurasikannya dengan **Ansible Playbook**. Jadi bisa dicek kode yang telah saya buat pada link ini [Nginx + Reverse proxy](https://gitlab.com/ledleledle/playbook-ansible/-/blob/master/install_nginx.yml)

Karena pada final task ini saya sudah mengetahui objektif yang akan saya kerjakan (deployment aplikasi, ci/cd, dll). Maka pada task ini, saya juga sekaligus melakukan reverse proxy.

Jadi saya melakukan installasi nginx lalu mengcopy template reverse proxy pada server public. 
```
- hosts: nginx
  tasks:
  - name: Install Nginx
    apt:
     name: nginx
     state: latest

  - name: Create New Folder
    file:
     path: /etc/nginx/leon
     state: directory

  - name: Copy template
    copy:
     src: template/tmp.conf
     dest: /etc/nginx/leon/{{item}}
     group: root
     owner: root
    loop:
     - front.conf
     - back.conf
     - jenkins.conf
     - monitor.conf
     - grafana.conf

  - name: Edit frontend
    lineinfile:
     path: /etc/nginx/leon/front.conf
     regexp: "{{ item.regexp }}"
     line: "{{ item.line }}"
    with_items:
     - { regexp: '^server_name', line: '       server_name leon.instructype.com;' }
     - { regexp: '^proxy_pass', line: '              proxy_pass http://backup;' }

  - name: Add Upstream Frontend
    blockinfile:
     path: /etc/nginx/leon/front.conf
     insertbefore: BOF
     block: |
       upstream backup {
               server 172.31.80.36:3000;
               server 172.31.80.103:3000;
          }

  - name: Edit backend
    lineinfile:
     path: /etc/nginx/leon/back.conf
     regexp: "{{ item.regexp }}"
     line: "{{ item.line }}"
    with_items:
     - { regexp: '^server_name', line: '       server_name api.leon.instructype.com;' }
     - { regexp: '^proxy_pass', line: '              proxy_pass http://172.31.80.213:5000;' }

  - name: Edit jenkins
    lineinfile:
     path: /etc/nginx/leon/jenkins.conf
     regexp: "{{ item.regexp }}"
     line: "{{ item.line }}"
    with_items:
     - { regexp: '^server_name', line: '       server_name cicd.leon.instructype.com;' }
     - { regexp: '^proxy_pass', line: '              proxy_pass http://172.31.80.14:8080;' }

  - name: Edit prometheus
    lineinfile:
     path: /etc/nginx/leon/monitor.conf
     regexp: "{{ item.regexp }}"
     line: "{{ item.line }}"
    with_items:
     - { regexp: '^server_name', line: '       server_name prometheus.leon.instructype.com;' }
     - { regexp: '^proxy_pass', line: '              proxy_pass http://172.31.80.7:9090;' }

  - name: Edit grafana
    lineinfile:
     path: /etc/nginx/leon/grafana.conf
     regexp: "{{ item.regexp }}"
     line: "{{ item.line }}"
    with_items:
     - { regexp: '^server_name', line: '       server_name monitoring.leon.instructype.com;' }
     - { regexp: '^proxy_pass', line: '              proxy_pass http://172.31.80.7:3000;' }


  - name: Add included file nginx.conf
    lineinfile:
     path: /etc/nginx/nginx.conf
     line: '        include /etc/nginx/leon/*;'
     insertafter: include.*

  - name: Restart Nginx
    service:
     name: nginx
     state: restarted
```

- Dapat dilihat pada konfigurasi ansible playbook yang saya buat. Pertama saya memasang paket nginx, lalu saya membuat folder yang akan berisi konfigurasi reverse proxy yang bernama `leon`.
- Sebelumnya saya telah membuat [template](https://gitlab.com/ledleledle/playbook-ansible/-/blob/master/template/tmp.conf). File ini akan dicopy menjadi beberapa file, diantaranya : frontend, backend, monitoring(prometheus & grafana) dan ci/cd.
- Selanjutnya yaitu saya melakukan perubahan pada data dengan mengisi ip, servername dll sesuai dengan kebutuhan server.

Berikut hasil dari ansible playbook yang sudah saya jalankan. Folder dan semua konfigurasi reverse proxy sudah berada pada tempatnya.<br>
<img src="ss/3.png">

Untuk isi dari konfigurasi, berikut saya akan menampilkan 1 sampel yaitu file `front.conf`.<br>
<img src="ss/5.png">

Untuk SSL saya lakukan secara manual, seperti pada gambar berikut.
<img src="ss/1.png">

SSL berhasil dipasang.<br>
<img src="ss/2.png">

Cek file reverse proxy yang sudah dipasang SSL.<br>
<img src="ss/4.png">