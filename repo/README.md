# Repository

Pertama saya membuat 2 repository, untuk aplikasi Dumbflix Frontend dan Backend pada repo github saya. Repository yang saya buat berstatus **Private**
<img src="ss/1.png">

Saya clone ke pc local saya.<br>
<img src="ss/2.png">

Saya menggunakan perintah `git remote set-url` untuk mengubah remote url untuk push dan pull. Saya lakukan pada 2 repo sekaligus.<br>
<img src="ss/3.png">

Buat branch dengan perintah `git branch *nana_branch*`. Lalu hasil dari pembuatan branch bisa cek dengan perintah `git branch -v`.<br>
<img src="ss/4.png">

Untuk push semua branch yang ada gunakan perintah `git push --all origin`.
<img src="ss/5.png">

Lalu cek pada repository github.

### Frontend
<img src="ss/6.png">

### Backend
<img src="ss/7.png">