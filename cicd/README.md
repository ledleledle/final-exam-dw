# CI/CD

Untuk CI/CD yang saya butuhkan yaitu Jenkins. Dan sekali lagi, semua paket sudah ter-cover pada konfigurasi Ansible yang sudah saya buat sebelumnya. [Docker Container](https://gitlab.com/ledleledle/playbook-ansible/-/blob/master/docker_container.yml).

```
- hosts: jenkins
  tasks:
  - name: jenkins volume dir
    file:
     path: /home/jenkins/jenkins_home
     state: directory
     owner: 1000
     group: 1000

  - name: Pull jenkins
    docker_image:
     name: jenkins/jenkins:lts
     pull: yes

  - name: Container jenkins
    docker_container:
     name: jenkins
     image: jenkins/jenkins
     ports:
      - 8080:8080
      - 50000:50000
     volumes: /home/jenkins/jenkins_home:/var/jenkins_home

```

- Pertama saya membuat folder volume untuk jenkins, untuk jenkins volume yang digunakan harus dimilik (ownership) jenkins (1000). Volume saya letakkan pada direktory home.
- Pull image jenkins dan menjalankan container jenkins pada port 8080 dan 50000

Setelah berhasil diinstall, masuk ke website jenkins. Untuk pertama kali jenkins akan meminta secret key, ini bisa dilihat melalui log docker atau melalui file seperti yang saya lakukan.
<img src="ss/2.png">

Selanjutnya pilih plugin yang tersedia. Lalu install (dilewati dulu juga tidak apa-apa).
<img src="ss/3.png">

Setelah pemasangan plugin selesai, buat user baru untuk login Jenkins.
<img src="ss/4.png">

Perfect... Sekarang saya sudah bisaa masuk ke halaman Home dari Jenkins.
<img src="ss/5.png">

Selanjutnya, saya perlu authorized_keys untuk setup menggunakan plugin `Publish Over SSH`. Pada task sebelumnya saya sudah melakukan copy paste ssh key pada server frontend dan backend. Untuk sekarang copy public key dan paste pada `authorized_keys`.<br>
<img src="ss/6.png">
<img src="ss/7.png">

Selanjutnya saya menambahkan list server untuk Publish Over SSH dengan 1 private key. Masuk ke menu `Manage Jenkins > Configure System`.
<img src="ss/8.png">

Untuk repository github karena sebelumnya saya menggunakan repository private, jadi saya membutuhkan ssh key untuk mendapatkan akses git. Tambahkan kredensial pada menu `Manage Jenkins > Manage Credentials`. SSH Key yang diisi disini yaitu Private Key.
<img src="ss/9.png">

Selanjutnya buat freestyle project untuk ke 3 server.
<img src="ss/10.png">

Source management saya pilih **Git**. Pilih SSH Key yang sudah ditambahkan.
<img src="ss/11.png">

Agar job bisa secara otomatis dibuild ketika ada push pada repository github, maka centang `Github Webhook`.<br>
<img src="ss/12.png">

Lalu pada tab Build, tambahakan publish over ssh dengan perintah untuk melakukan pull, dan penyegaran pada docker.<br>
<img src="ss/13.png">

Hampir lupa, untuk notifikasi build saya menggunakan Discord. Unduh terlebih dulu pluginnya.<br>
<img src="ss/14.1.png">

Pada aplikasi discord, buat server baru dan tambahkan webhook baru.
<img src="ss/14.2.png">

Setelah discord siap, copy link webhook. Lalu tambahkan pada Post Build Action.
<img src="ss/15.png">

Dan juga pada setiap repository saya menambahkan webhook yang tertuju pada domain jenkins saya.
<img src="ss/16.png">

Simpan semua konfigurasi.
<img src="ss/18.png">

Setelah mencoba melakukan build. Berikut hasil dari notifikasi yang dikirimkan oleh Jenkins pada Discord.
<img src="ss/17.png">