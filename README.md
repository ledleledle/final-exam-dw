# Final Exam DW

## Hasil :
- [Dumbflix](https://leon.instructype.com)
- [CI/CD Jenkins](https://cicd.leon.instructype.com)
- [Prometheus](https://prometheus.leon.instructype.com)
- [Grafana](https://monitoring.leon.instructype.com)

## Referensi :
#### SSH :
- https://www.thegeekstuff.com/2008/11/3-steps-to-perform-ssh-login-without-password-using-ssh-keygen-ssh-copy-id/
- https://www.digitalocean.com/community/tutorials/how-to-set-up-password-authentication-with-nginx-on-ubuntu-14-04

#### PostgreSQL :
- https://stackoverflow.com/questions/37694987/connecting-to-postgresql-in-a-docker-container-from-outside
- https://www.cyberciti.biz/faq/howto-add-postgresql-user-account/
- https://www.postgresqltutorial.com/postgresql-drop-database/

#### Grafana Template :
- https://grafana.com/grafana/dashboards/10242

#### Hostname :
- https://aws.amazon.com/premiumsupport/knowledge-center/linux-static-hostname/

#### Give me some relief :
- https://devconnected.com/how-to-install-prometheus-with-docker-on-ubuntu-18-04/#a_Prepare_the_Node_Exporter_for_Docker
- https://stackoverflow.com/questions/56948975/how-to-run-production-react-app-in-local-with-local-back-end
- https://nodejspedia.com/en/tutorial/2975/deploying-node-js-applications-in-production
- https://phoenixnap.com/kb/docker-cmd-vs-entrypoint
- https://dzone.com/articles/how-to-develop-your-nodejs-docker-applications-fas