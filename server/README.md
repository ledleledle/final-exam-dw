# Server

Pertama - tama saya menggunakan NAT instance untuk koneksi internet server private. Saya membuat 2 subnet yaitu public subnet dan private subnet.<br>
<img src="ss/1.png">

Selanjutnya saya membuat 2 *Route Table* private dan public. Untuk private tersambung dengan *Internet gateway* dari default VPC.<br>
<img src="ss/2.png">

Dan subnet asosiation tearah pada subnet public.
<img src="ss/3.png">

Selanjutnya, gunakan instance NAT dari comunity AMI.
<img src="ss/4.png">

Untuk Instance NAT Gunakan subnet public dan **Auto Asign IP** Enable.
<img src="ss/5.png">

Stop **Source/destination check** pada seluruh instance.
<img src="ss/6.png">

Setelah itu, untuk route table private sambungan routes dengan NAT Instance.
<img src="ss/7.png">

Dan subnet asosiation tearah pada subnet private.
<img src="ss/8.png">

Buat semua server yang dibutuhkan.
<img src="ss/9.png">

Untuk security group berikut daftarnya.

| Item          | Port         | Source             |
| ------------- | ------------ | ------------------ |
| Reverse proxy | 22,80,9100   | 0.0.0.0            |
| Backend       | 22,9100,5000 | 172.31.80.14, 172.31.80.7, 172.31.80.127, 172.31.26.82 |
| Frontend      | 22,3000,9100 | 172.31.80.14, 172.31.26.82, 172.31.80.7, 172.31.80.127 |
| Monitoring    | 22,9100,3000 | 172.31.26.82, 172.31.80.127 |
| Jenkins       | 22,9100,8080 | 172.31.80.7, 172.31.26.82, 172.31.80.127 |
| Database      | 5432,22,9100 | 172.31.80.213, 172.31.26.82, 172.31.80.7, 	172.31.80.127 |
| Ansible       | 22           | 172.31.26.82 |