# Final Exam DW

Untuk auth prometheus. Dibutuhkan paket dari apache-utils.
```
sudo apt-get install apache2-utils
```

Lalu generate file autentikasi passwordnya.
```
sudo htpasswd -c /etc/nginx/.htpasswd public
```

Tambahkan konfigurasi pada reverse proxy prometheus.
```
auth_basic           "Prometheus";
auth_basic_user_file /etc/nginx/.htpasswd;
```
<img src="ss/1.png">

Hasilnya, saat saya akan masuk ke website prometheus, saya akan dipromp password terlebih dahulu.<br>
<img src="ss/2.png">