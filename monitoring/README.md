# Monitoring

Pertama, install Prometheus, Node Exporter dan Grafana. Semua paket sudah ter-cover pada konfigurasi Ansible yang sudah saya buat sebelumnya. [Docker Container](https://gitlab.com/ledleledle/playbook-ansible/-/blob/master/docker_container.yml)

Untuk deploy Node exporter, berikut perintah dan penjelasannya.
```
docker run -d --net="host" --pid="host" -v "/:/host:ro,rslave" --name nodeex quay.io/prometheus/node-exporter --path.rootfs=/host
```
- **--net** membagi network yang sama dengan host.
- **-v** volume docker.
- **--path.rootfs** memetakan file sistem dari host, jadi node exporter tidak akan memetakan file sistem dari docker.

Untuk prometheus, saya membutuhkan konfigurasi pada file `prometheus.yml`. Jadi, file ini nanti akan disimpan pada direktori `template` dan akan dicopy pada server yang dituju(monitoring).
<img src="ss/1.png">

Berikut asible playbook saya untuk melakukan installasi Prometheus dan Grafana.
```
- hosts: monitoring
  tasks:
  - name: Make volume folder
    file:
     path: /home/monitoring/prometheus
     state: directory

  - name: Copy template
    copy:
     src: template/prometheus.yml
     dest: /home/monitoring/prometheus

  - name: Pull Prometheus
    docker_image:
     name: prom/prometheus
     pull: yes

  - name: Container Prometheus
    docker_container:
     name: prometheus
     image: prom/prometheus
     ports:
      - 9090:9090
     volumes: /home/monitoring/prometheus:/etc/prometheus

  - name: Pull Grafana
    docker_image:
     name: grafana/grafana    
     pull: yes

  - name: Container Grafana
    docker_container:
     name: grafana
     image: grafana/grafana
     ports:
      - 3000:3000
```

- Pertama, saya membuat folder untuk volume yang akan digunakan oleh Prometheus karena folder ini tidak akan secara otomatis terbuat.
- Copy template konfigurasi yang sebelumnya sudah dibuat.
- Pull prometheus dan grafana lalu menjalankannya dalam container.

#### Cek pada browser
Cek status server yang dimonitoring pada Prometheus.
<img src="ss/2.png">

Untuk Grafana, buat password baru (agar lebih aman).
<img src="ss/3.png">

Tambahkan data source pada Grafana. Tambahkan ip dari host monitoring, dan tidak perlu mengubah konfigurasi yang lain.
<img src="ss/4.png">

Jika sudah menambahkan ip, klik **Save & Test**.
<img src="ss/5.png">

Untuk grafana, saya menggunakan template, template ini sangat memudahkan dan sangat menghemat waktu, karena tidak perlu menambahkan metrics 1 per 1 pada Grafana.
<img src="ss/6.png">

Dan berikut hasilnya.
<img src="ss/7.png">

Jika melihat Host yang berupa ip pasti akan membingungkan pengguna, bahkan saya pun selaku yang melakukan installasi pada server-server tersebut masih kebingungan. Untuk itu saya akan mengubah konfigurasi prometheus agar setiap server memiliki nama yang berbeda-beda. Buka file `prometheus.yml`.<br>
<img src="ss/8.png">

Jadi dengan menambahkan hostname dengan nama yang berbeda-beda akan memudahkan pengguna dalam mengakses dan memonitoring server. Sekarang cek pada prometheus, apakah hostname sudah berhasil dikonfigurasikan. Namun sebelumnya restart docker terlebih dahulu untuk mendapatkan perubahan tersebut.
```
docker container restart prometheus
```

Bisa dilihat, pada prometheus sudah bertambah kolom `hostname`.
<img src="ss/9.png">

Sekarang pergi ke Grafana lalu pergi ke menu `Dashboard Settings > Variables`. Tambahkan kode dari metrics seperti berikut.
<img src="ss/10.png">

Maka Grafana sudah bisa menampilkan hostname sesuai dengan yang dikonfigurasikan pada file `prometheus.yml`.
<img src="ss/11.png">