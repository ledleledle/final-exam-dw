# User

Untuk setup user, saya menggunakan ansible.Buat user baru untuk server ansible.
<img src="ss/1.png">

Login ke seluruh server selain ansible (server yang akan diinstall & dikonfigurasi oleh ansible). Lalu edit file pada `/root/.ssh/authorized_keys`. Karena dari aws sendiri tidak diperbolehkan akses SSH menggunakan user root.
<img src="ss/2.png">

Lalu hapus kalimat berikut.
<img src="ss/3.png">

Install ansible pada server ansible.
```
sudo apt install software-properties-common
sudo apt-add-repository --yes --update ppa:ansible/ansible
sudo apt install ansible
```

Pada task ini, saya sekaligus menginstall docker. Kode yang saya pakai masih sama pada tugas minggu kemarin. Untuk installasi docker bisa cek file ansible yang telah saya buat [Docker install](https://gitlab.com/ledleledle/playbook-ansible/-/blob/master/install_docker.yml).<br>
<img src="ss/4.gif">

Setelah docker berhasil diinstall, langkah selanjutnya yaitu menambah user pada setiap server. Kode ansible playbook bisa dicek dilink berikut [Add User](https://gitlab.com/ledleledle/playbook-ansible/-/blob/master/add_user.yml). Kode ini akan membuat user ke seluruh server dengan nama user yang berbeda-beda, saya menyesuaikan kebutuhan.<br>
<img src="ss/5.gif">

Test masuk pada server yang telah ditambahkan usernya.
<img src="ss/6.gif">