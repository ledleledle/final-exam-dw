# Deployment

Pertama saya clone repository yang ada pada task pertama pada masing-masing server (2 frontend & backend). Menggunakan perintah `git clone`.

Lalu edit file `config/config.js` pada server backend dan edit file `src/config/api.js` pada server frontend.
<img src="ss/1.png">

Untuk server backend, copy `.env.example` menjadi `.env`.<br>
<img src="ss/2.png">

Untuk server backend, saya melakukan installasi nodejs juga diluar docker, yang saya gunakan untuk migrate database. Karena jika saya lakukan didalam image Docker, akan terjadi error.
```
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install -y nodejs
```

Lalu install sequelize-cli dan pg.
```
sudo npm install -g sequelize-cli pg
```

Agar `sequelize` dapat berjalan pada production, tambahkan environment variable baru yang bernama `NODE_ENV`. Dengan perintah `export NODE_ENV=production`. Lalu jalankan perintah `sequelize db:migrate` untuk melakukan migrasi database, perintah ini akan otomatis memigrasikan tabel dan data pada server database postgresql yang sudah dibuat sebelumnya.<br>
<img src="ss/7.png">

Environment yang diambil sequelize dari file `config/config.js` sudah berpindah ke production. Sekarang cek pada server database apakah tabel sudah dimigrasi.<br>
<img src="ss/8.png">

Buat dulu `Dockerfile` untuk frontend dan backend. Untuk production build, karena membutuhkan kecepatan dalam proses deploy maka production mode sangat dibutuhkan. Dalam kasus ini untuk server frontend menggunakan `redux`.
<img src="ss/9.png">

Untuk server backend production build menggunakan `nodemon`, paket ini sangat berguna karena setiap ada perubahan file, server akan otomatis merestart aplikasi.<br>
<img src="ss/10.png">

Setelah itu, Jalankan perintah docker build untuk membangun image.
#### Frontend
```
docker build -t leon0408/dffrontend .
```

#### Backend
```
docker build -t leon0408/dfbackend .
```

Lalu buat file `docker-compose.yml`. Untuk ini sama dengan task minggu sebelumnya, hanya mendefinisikan `container_name`, `image`, dan `port`. Untuk frontend saya membuka port 3000. Docker compose juga akan digunakan untuk proses build pada CI/CD Jenkins.<br>
<img src="ss/11.png">

Untuk `docker-compose` backend, saya membuka port 5000.<br>
<img src="ss/12.png">

Saya test dulu menggunakan perintah `docker-compose up` tanpa tag **-d** agar saya bisa melihat log yang ditampilkan. Untuk backend, bisa berjalan dengan normal.<br>
<img src="ss/13.png">

Untuk frontend juga tidak ada error. Maka semua server (backend, frontend 1, frontend 2) saya jalankan menggunakan mode daemon `docker compose up -d`.<br>
<img src="ss/14.png">

Cek pada link https://leon.instructype.com/
<img src="ss/5.png">

Dan domain untuk API juga.
<img src="ss/6.png">